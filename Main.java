package com.company;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        String  file_name = "File.txt";
        if (args.length > 0) {
            file_name = args[0];
        }
        BufferedReader br = null;
        try{
            File file = new File(file_name);
            if(!file.exists())
                file.createNewFile();
            br = new BufferedReader(new FileReader(file_name));
            String line;
            Commands stack = new Commands();
            while((line = br.readLine()) != null){
                switch (line.charAt(0)){
                    case ('D'): stack.define(line); break;
                    case ('P'):
                        switch (line.charAt(1)){
                            case ('O'): stack.pop(); break;
                            case ('R'): stack.print(); break;
                            case ('U'): stack.push(line); break;
                        }; break;
                    case ('*'): stack.multiplication(); break;
                    case ('-'): stack.subtraction(); break;
                    case ('/'): stack.division(); break;
                    case ('+'): stack.addition(); break;
                    case ('S'): stack.sqrt(); break;
                }
            }
        }
        catch (IOException e){
            System.out.print("ERROR" + e);
        }finally {
            try {
                br.close();
            }
            catch (IOException e){
                System.out.println("Error" + e);
            }
        }
    }
}
