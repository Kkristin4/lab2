package com.company;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Commands {
    Map map = new HashMap();
    ArrayList<Double> list = new ArrayList<Double>();

    public void define(String line) {
        String word = "";
        int value = 0;
        int i = 7;
        while (line.charAt(i) != ' ') {
            word = word + line.charAt(7);
            i++;
        }
        i++;
        for (int j = i++; j < line.length() ; j++){
            value = value * 10 + (int)line.charAt(j) - '0';
        }
        if (map.containsKey(word)) {
            map.replace(word, (int)value);
        }else{
            map.put(word, value);
        }
    }

    public void pop() {
        if (list.size() != 0){
            list.remove(list.size() - 1);
        }
    }

    public void print() {
        if (list.size() != 0)
            System.out.println(list.get(list.size() - 1));
    }

    public void push(String line) {
        Double number = 0.;
        String word = "";
        for (int i = 5; i < line.length(); i++){
            word = word + line.charAt(i);
        }
        if (map.containsKey(word)){
            line = "00000" + map.get(word);
        }
        for (int i = 5; i < line.length(); i++){
            number = number * 10 + line.charAt(i) - '0';
        }
        list.add(number);
    }

    public Double get_out(){
        Double number= list.get(list.size() - 1);
        pop();
        return number;
    }

    public void multiplication() {
        Double first_number = get_out();
        Double second_number = get_out();
        list.add(first_number * second_number);

    }

    public void subtraction() {
        Double first_number = get_out();
        Double second_number = get_out();
        list.add(first_number - second_number);
    }

    public void division() {
        Double first_number = get_out();
        Double second_number = get_out();
        list.add(first_number / second_number);
    }

    public void addition() {
        Double first_number = get_out();
        Double second_number = get_out();
        list.add(first_number + second_number);
    }

    public void sqrt() {
        Double number = get_out();
        list.add(Math.sqrt(number));
    }
}
